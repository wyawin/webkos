<style>
.command{
	font-style:italic;
	color:#bbb;
	
}
</style>
<?php
echo("<title>Webkos</title>");
$table = 5;
function place($x,$y,$f){
	$cars = array();
	$cars["x"] = $x;
	$cars["y"] = $y;
	$cars["f"] = $f;
	
	return $cars;
}
function move($x,$y,$f){
	$cars = array();
	if($f == "N"){
		$y += 1;
		$cars["x"] = $x;
		$cars["y"] = $y;
		$cars["f"] = $f;
		
	} else if($f == "W"){
		$x -= 1;
		$cars["x"] = $x;
		$cars["y"] = $y;
		$cars["f"] = $f;
	} else if($f == "E"){
		$x += 1;
		$cars["x"] = $x;
		$cars["y"] = $y;
		$cars["f"] = $f;
	} else if($f == "S"){
		$y -= 1;
		$cars["x"] = $x;
		$cars["y"] = $y;
		$cars["f"] = $f;
	}
	if($x < $GLOBALS['table'] && $y < $GLOBALS['table'] && $x >= 0 && $y >= 0){
		return $cars;
	} else {
		return false;
	}
}
function face($dir,$x,$y,$curFace){
	if($dir == "LEFT"){
		if($curFace == "N"){
			$curFace = "W";
			$cars["x"] = $x;
			$cars["y"] = $y;
			$cars["f"] = $curFace;
		} else if($curFace == "W"){
			$curFace = "S";
			$cars["x"] = $x;
			$cars["y"] = $y;
			$cars["f"] = $curFace;
		} else if($curFace == "S"){
			$curFace = "E";
			$cars["x"] = $x;
			$cars["y"] = $y;
			$cars["f"] = $curFace;
		} else if($curFace == "E"){
			$curFace = "N";
			$cars["x"] = $x;
			$cars["y"] = $y;
			$cars["f"] = $curFace;
		}
	} else if($dir == "RIGHT"){
		if($curFace == "N"){
			$curFace = "E";
			$cars["x"] = $x;
			$cars["y"] = $y;
			$cars["f"] = $curFace;
		} else if($curFace == "E"){
			$curFace = "S";
			$cars["x"] = $x;
			$cars["y"] = $y;
			$cars["f"] = $curFace;
		} else if($curFace == "S"){
			$curFace = "W";
			$cars["x"] = $x;
			$cars["y"] = $y;
			$cars["f"] = $curFace;
		} else if($curFace == "W"){
			$curFace = "N";
			$cars["x"] = $x;
			$cars["y"] = $y;
			$cars["f"] = $curFace;
		}
	}
	return $cars;
}
function getString($f){
	switch($f){
		case "N":
			$str = "North";
			break;
		case "E":
			$str = "East";
			break;
		case "S":
			$str = "South";
			break;
		case "W":
			$str = "West";
			break;
	}
	return $str;
}
function go($com,$x,$y,$f){
	
	$explodeCommand = explode(",",$com);
	$n = place($x,$y,$f);
	echo "Command : " . $com . "<br/><br/>";
	echo "<ul>";
	echo "<li>" . $n["x"] . ", " .  $n["y"] . ", " . getString($n["f"]) . "</li>";
	if($x > $GLOBALS["table"] - 1 && $y > $GLOBALS["table"] - 1 && $x >= 0 && $y >= 0){
		echo " Not a valid PLACE";
		echo "</ul>";
	} else {
		foreach($explodeCommand as $ex){
			$ex = trim($ex);
			if($ex == "MOVE"){
				$n = move($n["x"],$n["y"],$n["f"]);
			} else if(strtoupper($ex) == "RIGHT" || strtoupper($ex) == "LEFT"){
				$n = face(strtoupper($ex),$n["x"],$n["y"],$n["f"]);
			} else {
				echo "Invalid Command";
				return false;
			}
			if($n){
				echo "<li>" . $n["x"] . ", " .  $n["y"] . ", " . getString($n["f"]) . "</li>";
			} else {
				echo "</ul> Car is at the far end of the table";
				return false;
			}
		}
		echo "</ul>";
	}
}
?>
<form action="index.php" method="post">
	<input type="number" min="0" max="4" name="x" placeholder="X" required />
	<input type="number" min="0" max="4" name="y" placeholder="Y" required />
	<select name="face" required>
		<option value="N">North</option>
		<option value="E">East</option>
		<option value="S">South</option>
		<option value="W">West</option>
	</select>
	<input type="text" name="command" placeholder="Command" required />
	<button type="submit">GO</button>
</form>
<ul class="command">
	<li>X (Min = 0, Max = 4)</li>
	<li>X (Min = 0, Max = 4)</li>
	<li>Face (North, East, South, West)</li>
	<li>Command (Move, Left, Right)</li>
</ul>
<?php
if(isset($_POST['command'])){
	$com = $_POST['command'];
	$x = $_POST['x'];
	$y = $_POST['y'];
	$f = $_POST['face'];
	
	go($com,$x,$y,$f);
}
?>